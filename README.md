# TranscriptAudioAPI

This workflow performs a Smarthis Hub API call to produce a transcription from a audio URL common in the acessibility tools for ReCaptcha Solutions.

## Description
This workflow was developed by using the API documentation at [https://audio-api.hub.smarthis.com.br/docs#/](https://audio-api.hub.smarthis.com.br/docs#/).
It Authenticates in the API service and request the Audio trancription that is the output as a String, also veryfing the requests status that if were not succcess it will throw a exception.

## Installation
Paste this file in the project folder, then use a Invoke Workflow activity.

## Usage
The arguments are:
- in_intHttpTimeOut : time out for the api http requests;
- in_strUsername : Registered credential username;
- in_strPassword : Registered credential password;
- in_strAudioUrl : Url of the Audio to transcript;
- out_strAudioTranscript : string containing the transcription of the audio.

The exception messages will indicate the response status and it's detail also the source of the exception indicates if it was a autentication error as "Audio-API Auth error" or a trancription error as "Audio-API Transcript error"

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/GARocha/transcriptaudioapi.git
git branch -M main
git push -uf origin main
```
